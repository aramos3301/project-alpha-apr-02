from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import CreateProject

# from django.contrib.auth.mixins import LoginRequiredMixin
# from django.views.generic.list import ListView


# @login_required
# class ProjectListView(LoginRequiredMixin, ListView):
#     model = Project
#     template_name = "list.html"

#     def get_queryset(request):
#         return Project.objects.filter(owner=request.user)


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": projects,
    }
    return render(request, "projects/list.html", context)


def show_project(request, pk):
    projects = Project.objects.get(pk=pk)
    context = {
        "show_project": projects,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateProject(request.POST)
        if form.is_valid():
            form.save()
            Project = form.save(False)
            Project.owner = request.user
            Project.save()
            return redirect("list_projects")
    else:
        form = CreateProject()

    context = {"form": form}

    return render(request, "projects/create.html", context)
