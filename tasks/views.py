from django.shortcuts import render, redirect
from tasks.forms import CreateTask
from django.contrib.auth.decorators import login_required

from tasks.models import Task


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTask(request.POST)
        if form.is_valid():
            form.save()
            Project = form.save()
            Project.owner = request.user
            Project.save()
            return redirect("list_projects")
    else:
        form = CreateTask()

    context = {"form": form}

    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.all()
    context = {
        "show_my_tasks": tasks,
    }
    return render(request, "tasks/mine.html", context)


# variable links to task list = show_my_tasks
